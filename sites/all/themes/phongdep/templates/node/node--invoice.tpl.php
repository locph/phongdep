<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
  global $base_url;

  $room = node_load($node->field_room['und'][0]['target_id']);
  $house = node_load($room->field_house['und'][0]['target_id']);
  $district = node_load($house->field_district['und'][0]['target_id']);

  // room
  $currency = taxonomy_term_load($room->field_currency['und'][0]['tid'])->description;
  $currency = ' '. mb_substr($currency, 0, 1);

  // contract
  $room_price            = $room->field_room_price['und'][0]['value'];
  $internet_price        = $room->field_internet_price['und'][0]['value'];
  $electricity_price     = $room->field_electricity_price['und'][0]['value'];
  $water_price           = $room->field_water_price['und'][0]['value'];
  $washing_machine_price = $room->field_washing_machine_price['und'][0]['value'];
  $elevator_price        = $room->field_elevator_price['und'][0]['value'];
  $cable_price           = $room->field_cable_price['und'][0]['value'];
  $late_payment          = $room->field_late_payment['und'][0]['value'];
  $deposit               = $room->field_deposit['und'][0]['value'];
  $person                = $room->field_person['und'][0]['value'];

  // invoice
  $old_electricity = $node->field_old_electricity['und'][0]['value'];
  $new_electricity = $node->field_new_electricity['und'][0]['value'];
  $deposit_back    = $node->field_deposit_back['und'][0]['value'];
  $other_price     = $node->field_other_price['und'][0]['value'];
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <div class="content"<?php print $content_attributes; ?>>
    <div id="invoice-print">
      <header>
        <img src="<?php echo $base_url . '/' . path_to_theme() ?>/images/logo_invoice.jpg" width="90" height="90" />
        <div id="company-info">
          <h4><?php echo variable_get('phongdep_company_name'); ?></h4>
          <div><?php echo variable_get('phongdep_company_address'); ?>, TP. <?php echo taxonomy_term_load(variable_get('phongdep_company_city'))->name ?></div>
          <div>
            Điện thoại: <?php echo variable_get('phongdep_company_phone'); ?>
            | Email: <?php echo variable_get('phongdep_company_email'); ?>
            | Website: <?php echo variable_get('phongdep_company_website'); ?>
          </div>
        </div>
        <?php print render($title_prefix); ?>
        <?php if (!$page): ?>
          <h2<?php print $title_attributes; ?>><?php print $title; ?></h2>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
      </header>

      <table id="invoice-customer">
        <tr>
          <td>
            <div>Khách hàng: <b><?php echo $room->field_customer_name['und'][0]['value']; ?></b></div>
            <div>Phòng: <?php echo $room->title ?>. Nhà: <?php echo $house->title ?></div>
            <div>Địa chỉ: <?php echo $house->field_address['und'][0]['value']; ?>, Q.<?php echo $district->title ?>, TP. <?php echo taxonomy_term_load($district->field_city['und'][0]['tid'])->name ?></div>
          </td>
          <td valign="top" align="right">
            <div>Hóa đơn #<?php echo $node->nid ?>, ngày <?php echo date('d/m/Y') ?></div>
          </td>
        </tr>
      </table>

      <table id="invoice-detail">
        <thead>
          <tr>
            <th>Mô tả</th>
            <th>Đơn giá</th>
            <th>Thành tiền</th>
          </tr>
        </thead>
        <tbody>
          <?php if (!$deposit_back): ?>
          <tr>
            <td>Giá thuê phòng</td>
            <td><?php echo number_format($room_price).$currency ?></td>
            <td><?php echo number_format($room_price).$currency ?></td>
          </tr>
          <?php endif ?>
          <tr>
            <td>
              Điện: <?php echo ($new_electricity - $old_electricity) ?> KWh<br/>
              (Số điện cũ: <?php echo $old_electricity ?> KWh; Số điện mới: <?php echo $new_electricity ?> KWh)
            </td>
            <td><?php echo number_format($electricity_price).$currency ?>/KWh</td>
            <td><?php echo number_format(($new_electricity - $old_electricity)*$electricity_price).$currency ?></td>
          </tr>
          <tr>
            <td>Nước (<?php echo $person ?> người)</td>
            <td><?php echo number_format($water_price).$currency ?>/người</td>
            <td><?php echo number_format($person*$water_price).$currency ?></td>
          </tr>

          <?php
            $feature = array(
              'Internet' => $internet_price,
              'Máy giặt' => $washing_machine_price,
              'Truyền hình cáp' => $cable_price,
              'Thang máy' => $elevator_price,
              'Internet' => $internet_price,
              "other" => $other_price
            );
          ?>

          <?php foreach ($feature as $key => $value): ?>
            <?php if ($value): ?>
            <tr>
              <td><?php echo $key == 'other' ? render($node->body['und'][0]['value']) : $key; ?></td>
              <td><?php echo number_format($value).$currency ?></td>
              <td><?php echo number_format($value).$currency ?></td>
            </tr>
            <?php endif ?>
          <?php endforeach ?>
        </tbody>
      </table>

      <table id="invoice-total">
        <tr>
          <td>
            <?php if (!$deposit_back): ?>
              <div>Số tiền cọc còn lại: <?php echo number_format($deposit).$currency ?></div>
              <div>Số ngày trễ hẹn còn lại: <?php echo $late_payment ?> ngày</div>
            <?php endif ?>

            <?php if ($deposit_back): ?>
              <div>Tổng thành tiền: {{ (totaldue + contract.deposit)|money }}</div>
              <div>Tiền cọc: <?php echo number_format($deposit).$currency ?> (hoàn lại khi trả phòng)</div>
            <?php endif ?>
          </td>
          <td valign="middle" align="right">
            <div><b>Tổng số tiền thanh toán: {{ totaldue|money }}</b></div>
          </td>
        </tr>
      </table>

      <div id="invoice-description">
        <h4>Thời hạn thanh toán</h4>
        <p>
          Xin Quý khách hàng vui lòng thanh toán tiền
          thuê phòng trễ nhất vào hạn cuối
          <strong><?php echo $house->field_invoice_day['und'][0]['value'].date('/m/Y', $node->changed) ?></strong>.
        </p>

        <h4>Hình thức thanh toán</h4>
        <ol>
          <li>
            <p>
              Chuyển khoản ngân hàng:
                <?php echo variable_get('phongdep_bank_name') ?>, Phòng giao dịch
                <?php echo variable_get('phongdep_bank_trading_room') ?>, Chi nhánh
                <?php echo variable_get('phongdep_bank_branch') ?>. Tên tài khoản:
                <?php echo variable_get('phongdep_bank_account') ?>, Số tài khoản:
                <?php echo variable_get('phongdep_bank_id') ?>.
            </p>
            <p>
              <b>Lưu ý</b>: Khi chuyển tiền, quý khách ghi phần
              <strong>nội dung chuyển tiền</strong> theo cú pháp sau:
              <strong>TIEN PHONG <?php echo $room->title ?>,
              <?php echo date('m/Y', $node->changed) ?></strong>.
              Chi phí giao dịch chuyển khoản khác ngân hàng sẽ được
              công ty chi trả.
            </p>
          </li>
          <li>
            <p>
              Thanh toán tại phòng:
              Giờ làm việc: 8h - 21h (T2 - T7).
              Phí thu tiền: <?php echo number_format(30000) ?>/phòng.
              Liên hệ số điện thoại <?php echo variable_get('phongdep_company_phone') ;?>
              để yêu cầu nhân viên đến thu tiền tại phòng.
            </p>
          </li>
        </ol>

        <h4>Thanh toán trễ hẹn</h4>
        <p>
          Quý khách hàng được thanh toán trễ hẹn số ngày tối đa được quy định theo thời gian ký hợp đồng.
          Mỗi ngày thanh toán trễ quý khách phải chi trả khoản tiền phạt bằng giá trị tiền thuê phòng một ngày.
          Khoản tiền phạt sẽ được trừ trực tiếp vào số tiền cọc của quý khách và thông báo qua hoá đơn hàng tháng.
          Quý khách trễ quá tổng số ngày quy định trong hợp đồng sẽ bị thu hồi lại phòng và mất số tiền đã đặt cọc.
        </p>
      </div>

      <?php // dpm($content) ?>
      <?php echo render($content['links']['print_html']) ?>
      <?php echo render($content['links']['print_mail']) ?>
    </div>
  </div>
</div>
