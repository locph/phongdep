<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */

  $room = array();
  $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
  $link = url($path, array('absolute' => TRUE));
  $manager = user_load($node->field_house['und'][0]['entity']->field_manager['und'][0]['target_id']);
  $expiration = isset($node->field_rental_duration['und']) ? $node->field_rental_duration['und'][0]['value2'] : null;
  $remain_date = phongdep_remain_date($expiration);
  $features_include = isset($node->field_features['und']) ? $node->field_features['und'] : null;
  $features_include_key = array();
  $results = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'feature', '=')
    ->execute()
    ->fetchCol();
  $features = node_load_multiple($results);
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div id="house-info" class="box">
        <h2>Thông tin nhà</h2>

        <table class="table">
          <tr>
            <td>Tên nhà</td>
            <td><?php echo $node->field_house['und'][0]['entity']->title ?></td>
          </tr>
          <tr>
            <td>Địa chỉ</td>
            <td>
              <?php echo $node->field_house['und'][0]['entity']->field_address['und'][0]['value'] ?>,
              <?php
                // district
                $district_id = $node->field_house['und'][0]['entity']->field_district['und'][0]['target_id'];
                $result = db_query('select n.title from {node} n where nid = :nid', array(
                  'nid' => $district_id
                ))->fetchObject();
                echo 'Q.' . $result->title;
              ?>
            </td>
          </tr>
          <tr>
            <td>Thành phố</td>
            <td>
              <?php
                // city
                $result = db_query('select t.name
                    from {taxonomy_term_data} t, {field_data_field_city} c
                    where t.tid = c.field_city_tid
                    and c.entity_id = :entity_id
                    and c.bundle = :bundle', array(
                  'entity_id' => $district_id,
                  'bundle' => 'district',
                ))->fetchObject();
                echo $result->name;
              ?>
            </td>
          </tr>
          <tr>
            <td>Phòng trống</td>
            <td>
              <?php
                $house_id = $node->field_house['und'][0]['target_id'];
                $results = db_select('field_data_field_house', 'a')
                  ->fields('a', array('entity_id'))
                  ->condition('field_house_target_id', $house_id, '=')
                  ->execute()->fetchCol();
                $rooms = node_load_multiple($results);
                // var_dump($rooms);
                $free_rooms = 0;
                foreach ($rooms as $key => $value) {
                  if (isset($value->field_rental_duration['und'][0]['value2'])) {
                    $contract_end = strtotime($value->field_rental_duration['und'][0]['value2']);
                    $now = strtotime(date('Y-m-d H:s:i'));
                    if ($contract_end < $now) {
                      $free_rooms++;
                    }
                  }
                  else {
                    $free_rooms++;
                  }
                }
                echo $free_rooms . '/';

                $results = db_select('field_data_field_house', 'a')
                  ->fields('a')
                  ->condition('field_house_target_id', $house_id, '=')
                  ->execute()->rowCount();
                echo $results;
              ?>
            </td>
          </tr>
          <tr>
            <td>Chỗ để xe</td>
            <td><?php echo $node->field_house['und'][0]['entity']->field_parking['und'][0]['value'] ?></td>
          </tr>

          <?php if (isset($node->field_house['und'][0]['entity']->field_feature_house['und'])): ?>
          <tr>
            <td>Tiện ích chung</td>
            <td>
              <?php
                $feature_house = $node->field_house['und'][0]['entity']->field_feature_house['und'];
                foreach ($feature_house as $key => $value) {
                  $separator = ($key + 1) < count($feature_house) ? ', ' : '';
                  $result = db_select('node', 'n')
                    ->fields('n')
                    ->condition('nid', $value['target_id'], '=')
                    ->range(0,1)
                    ->execute()
                    ->fetchAssoc();
                  echo $result['title'] . $separator;
                }
              ?>
            </td>
          </tr>
          <?php endif ?>

          <tr>
            <td colspan="2">
                <a href="<?php echo url('node/' . $node->field_house['und'][0]['target_id'], array('ABSOLUTE' => TRUE)); ?>" class="btn btn-success"><i class="icon-chevron-right icon-white"></i> Chi tiết</a>
            </td>
          </tr>
        </table>
    </div>

    <div id="room-info" class="box">
      <h2>Thông tin phòng <?php echo $node->title ?></h2>
      <div id="price">
        Giá thuê:
        <span id="rental-price">
          <?php echo number_format($node->field_price['und'][0]['value'],0,',','.') ?>
        </span> đ
      </div>

      <ul id="room-info-detail">
        <!-- <li>Thời hạn hợp đồng:
          <select name="period" id="period">
            <option value="1.25">1 tháng</option>
            <option value="1.15">3 tháng</option>
            <option value="1" selected>6 tháng</option>
          </select>
        </li> -->
        <li>
          <label for="room-area">Diện tích:</label>
          <div id="room-area"><?php echo $node->field_area['und'][0]['value'] ?> m2</div>
        </li>
        <li>
          <label for="room-status">Tình trạng phòng:</label>
          <div id="room-status">
          <?php echo ($remain_date > 0) ? "Đang cho thuê ($remain_date ngày nữa trống)" : "Đang trống" ?>
          </div>
        </li>
        <li>
          <label for="room-electric">Tiền điện:</label>
          <div id="room-electric">3000 đ/KWh</div>
        </li>
        <li>
          <label for="room-water">Tiền nước:</label>
          <div id="room-water">100.000 đ/người</div>
        </li>
        <li>
          <label for="room-wifi">Wifi:</label>
          <div id="room-wifi">100.000 đ</div>
        </li>
        <li>
          <label for="room-tv">Truyền hình cáp:</label>
          <div id="room-tv">50.000 đ</div>
        </li>
        <li>
          <label for="room-washing">Máy giặt:</label>
          <div id="room-washing">100.000 đ</div>
        </li>
      </ul>
    </div>

    <div id="room-feature-include" class="box">
      <h2>Tiện nghi đã bao gồm</h2>
      <ul id="features">
        <?php foreach ($features_include as $key => $feature): ?>
        <li>
          <span class="feature-name">
            <?php echo node_load($feature['target_id'])->title ?>
            <?php $features_include_key[] = $feature['target_id'] ?>
          </span>
        </li>
        <?php endforeach ?>
      </ul>
    </div>

    <div id="room-feature-no-include" class="box">
      <h2>Tiện nghi tuỳ chọn</h2>
      <p>
          Ngoài những tiện nghi đã có sẵn, với hợp đồng từ 6 tháng trở lên,
          bạn có thể tuỳ chọn thêm những tiện nghi mới.
          Việc tuỳ chọn tiện nghi có thể làm thay đổi giá thuê
          phòng của bạn.
      </p>
      <!-- <div id="last-price">
          Giá thuê cuối cùng: <span></span> đ/tháng
      </div> -->
      <ul id="features">
        <?php foreach ($features as $key => $feature): ?>
          <?php
            $feature_type = $feature->field_feature_category['und'][0]['value'];
            $feature_price = $feature->field_price['und'][0]['value'];
          ?>
          <?php if ($feature_type == 'room' && $feature_price > 0 && !in_array($key, $features_include_key)): ?>
          <li>
            <span class="feature-name"><?php echo $feature->title ?></span>
            (<span class="feature-price"><?php echo number_format($feature_price) ?></span> đ/tháng)
          </li>
          <?php endif ?>
        <?php endforeach ?>
      </ul>
    </div>

    <div id="contact-phone" class="box">
      <h2>Liên hệ thuê phòng trực tiếp</h2>
      <table class="table">
        <tr>
          <td><i class="icon-user"></i> Người liên hệ</td>
          <td><?php echo $manager->field_fullname['und'][0]['value'] ?></td>
        </tr>
        <tr>
          <td><i class="icon-comment"></i> Số điện thoại</td>
          <td><?php echo $manager->field_phone['und'][0]['value'] ?></td>
        </tr>
      </table>
    </div>

    <!-- <div id="contact-online" class="box">
      <h2>Đặt phòng Online</h2>
      <?php
        $block = module_invoke('webform', 'block_view', 'client-block-158');
        echo render($block);
      ?>
    </div> -->

    <?php if ($node->body): ?>
    <div id="" class="box item-description">
      <?php echo render($content['body']) ?>
    </div>
    <?php endif ?>

    <div id="facebook-comment" class="box facebook-comment">
      <h2>Chia sẻ với bạn bè</h2>
      <div class="fb-like"
        data-href="<?php echo $link ?>"
        data-send="true"
        data-width="450"
        data-show-faces="true">
      </div>
      <div class="fb-comments"
        data-href="<?php echo $link ?>"
        data-width="960"
        data-num-posts="10">
      </div>
    </div>
  </div>
</div>
