<?php

/**
 * @file
 * Default theme implementation to display a node.
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 *
 * @ingroup themeable
 */
  $district = node_load($node->field_district['und'][0]['target_id']);
  $path = isset($_GET['q']) ? $_GET['q'] : '<front>';
  $link = url($path, array('absolute' => TRUE));
  $map = isset($node->field_map['und']) ? $node->field_map['und'][0]['value'] : null;
  $features = isset($node->field_feature_house['und']) ? $node->field_feature_house['und'] : null;
  $manager = user_load($node->field_manager['und'][0]['target_id']);
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <div id="house-info" class="box">
      <h2>Thông tin nhà <?php echo $title ?></h2>
      <table class="table">
        <tr>
            <td>Địa chỉ</td>
            <td>
                <?php echo $node->field_address['und'][0]['value'] ?>,
                Q. <?php echo $district->title ?>,
                TP. <?php echo taxonomy_term_load($district->field_city['und'][0]['tid'])->name; ?>

                <?php if ($map): ?>
                  <p>
                    <a href="<?php echo $map ?>" class="btn btn-success" id="house-map" target="_blank">
                      <i class="icon-map-marker icon-white"></i> Xem bản đồ
                    </a>
                  </p>
                <?php endif; ?>
            </td>
        </tr>
        <tr>
            <td>Chỗ để xe</td>
            <td><?php echo render($content['field_parking']) ?></td>
        </tr>
        <tr>
            <td>Phòng trống</td>
            <td>
              <?php
                $results = db_select('field_data_field_house', 'a')
                  ->fields('a', array('entity_id'))
                  ->condition('field_house_target_id', $node->nid, '=')
                  ->execute()->fetchCol();
                $rooms = node_load_multiple($results);
                $free_rooms = 0;
                foreach ($rooms as $key => $room) {
                  if (isset($room->field_rental_duration['und'][0]['value2'])) {
                    $contract_end = strtotime($room->field_rental_duration['und'][0]['value2']);
                    $now = strtotime(date('Y-m-d H:s:i'));
                    if ($contract_end < $now) {
                      $free_rooms++;
                    }
                  }
                  else {
                    $free_rooms++;
                  }
                }
                echo $free_rooms . '/';

                $results = db_select('field_data_field_house', 'a')
                  ->fields('a')
                  ->condition('field_house_target_id', $node->nid, '=')
                  ->execute()->rowCount();
                echo $results;
              ?>
            </td>
        </tr>

        <?php if (isset($features)): ?>
        <tr>
          <td>Tiện ích chung</td>
          <td>
            <?php
              foreach ($features as $key => $feature) {
                $separator = ($key + 1) < count($features) ? ', ' : '';
                echo node_load($feature['target_id'])->title . $separator;
              }
            ?>
          </td>
        </tr>
        <?php endif ?>

        <?php if (isset($manager)): ?>
        <tr>
          <td>Liên hệ</td>
          <td>
            <a href="mailto:<?php echo $manager->mail ?>"><?php echo $manager->field_fullname['und'][0]['value'] ?></a>
            (<?php echo $manager->field_phone['und'][0]['value'] ?>)
          </td>
        </tr>
      <?php endif ?>
      </table>
    </div>

    <?php if ($node->body): ?>
      <div class="box item-description">
        <?php echo render($content['body']) ?>
      </div>
    <?php endif; ?>

    <div id="facebook-comment" class="box facebook-comment">
      <h2>Chia sẻ với bạn bè</h2>
      <div class="fb-like"
        data-href="<?php echo $link ?>"
        data-send="true"
        data-width="450"
        data-show-faces="true">
      </div>
      <div class="fb-comments"
        data-href="<?php echo $link ?>"
        data-width="960"
        data-num-posts="10">
      </div>
    </div>
  </div>
</div>
