<?php
/**
  * @file
  * Contact block template
  */

?>

<div id="contact" class="section">
  <div id="heartquarter" class="box">
      <h2>Công ty cổ phần Bất Động Sản BIG LAND</h2>
      <ul class="content">
          <li class="ico-home">Văn phòng PhòngĐẹp.VN TP.HCM</li>
          <li class="ico-map">
              Địa chỉ: Tẩng 4, Toà nhà Think BIG, 360 Lý Thái Tổ, Phường 1, Quận 3, TP. HCM
          </li>
          <li class="ico-email">Email: <a href="mailto:lienhe@bigland.vn">lienhe@bigland.vn</a></li>
          <li class="ico-facebook">
              Facebook: <a href="//facebook.com/phongchothue.hcm" alt="facebook phongdep.vn" target="_blank">facebook.com/phongchothue.hcm</a>
          </li>
          <li class="ico-phone">
              Liên hệ thuê phòng, gửi nhà cho thuê :
              Hotline: 082.246.3091
          </li>
          <li class="ico-phone">
              Liên hệ hỗ trợ trong quá trình ở :
              Hotline: 086.684.3091
          </li>
          <li class="ico-person">
              Giám Đốc Kinh Doanh:
              Mr. <a href="//facebook.com/vophinhatquang" target="_blank">Võ Phi Nhật Quang</a> (090.639.7484)
          </li>
          <li class="ico-person">
              Tổng Giám Đốc (C.E.O)
              Mr. <a href="//facebook.com/vophinhathuy" target="_blank">Võ Phi Nhật Huy</a> (096.814.5410)
          </li>
      </ul>
  </div>

  <div id="contact-map" class="box">
      <h2>Bản đồ</h2>
      <img src="<?php echo drupal_get_path('module', 'contact_block') . '/map.jpg' ?>" width="450" height="270" alt="Big Land office map" />
      <p>
        <a class="btn btn-success"
          id="google-map"
          href="http://goo.gl/maps/Z4Sw5"
          target="_blank">
          <i class="icon-map-marker icon-white"></i> Xem bản đồ
        </a>
      </p>
  </div>
</div>