<?php
  function phongdep_remain_date($from) {
    $from = strtotime($from);
    $to = strtotime(date('Y-m-d H:s:i'));
    $diff = $from - $to;
    $days = floor($diff / (60*60*24));

    return ($days > 0 ? $days : 0);
  }

  /**
   * Menu callback: Facebook setting
   */
  function phongdep_admin_company_settings($form, &$form_state) {
    // Global settings for the whole module.
    $form['phongdep_company_global'] = array(
      '#type' => 'fieldset',
      '#title' => t('Company Settings'),
      '#collapsible' => TRUE,
    );

    $form['phongdep_company_global']['phongdep_company_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Company name'),
      '#default_value' => variable_get('phongdep_company_name', NULL),
      '#description' => t('Your company name'),
      '#required' => TRUE,
    );

    $form['phongdep_company_global']['phongdep_company_address'] = array(
      '#type' => 'textarea',
      '#title' => t('Address'),
      '#default_value' => variable_get('phongdep_company_address', NULL),
      '#description' => t('Your company address'),
      '#required' => TRUE,
    );

    //get the list of citys
    $dropdown_source = taxonomy_get_tree(2);
    foreach ($dropdown_source as $item) {
      $key = $item->tid;
      $value = $item->name;
      $dropdown_array[$key] = $value;
    }
    $form['phongdep_company_global']['phongdep_company_city'] = array(
      '#type' => 'select',
      '#title' => t('City'),
      '#default_value' => variable_get('phongdep_company_city', NULL),
      '#options' => $dropdown_array,
      '#description' => t('The default theme to use for comments.'),
      '#required' => TRUE,
    );

    $form['phongdep_company_global']['phongdep_company_phone'] = array(
      '#type' => 'textfield',
      '#title' => t('Phone'),
      '#default_value' => variable_get('phongdep_company_phone', NULL),
      '#description' => t('Your company phone'),
      '#required' => TRUE,
    );

    $form['phongdep_company_global']['phongdep_company_email'] = array(
      '#type' => 'textfield',
      '#title' => t('Email'),
      '#default_value' => variable_get('phongdep_company_email', NULL),
      '#description' => t('Your company email'),
      '#required' => TRUE,
    );

    $form['phongdep_company_global']['phongdep_company_facebook'] = array(
      '#type' => 'textfield',
      '#title' => t('Facebook'),
      '#default_value' => variable_get('phongdep_company_facebook', NULL),
      '#description' => t('Your company facebook'),
      '#required' => FALSE,
    );

    $form['phongdep_company_global']['phongdep_company_website'] = array(
      '#type' => 'textfield',
      '#title' => t('Website'),
      '#default_value' => variable_get('phongdep_company_website', NULL),
      '#description' => t('Your company website'),
      '#required' => TRUE,
    );

    return system_settings_form($form);
  }

  /**
   * Menu callback: Bank setting
   */
  function phongdep_admin_bank_settings($form, &$form_state) {
    // Global settings for the whole module.
    $form['phongdep_bank_global'] = array(
      '#type' => 'fieldset',
      '#title' => t('Bank Settings'),
      '#collapsible' => TRUE,
    );

    $form['phongdep_bank_global']['phongdep_bank_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank'),
      '#default_value' => variable_get('phongdep_bank_name', NULL),
      '#description' => t('Your bank name'),
      '#required' => TRUE,
    );

    $form['phongdep_bank_global']['phongdep_bank_trading_room'] = array(
      '#type' => 'textfield',
      '#title' => t('Trading room'),
      '#default_value' => variable_get('phongdep_bank_trading_room', NULL),
      '#description' => t('Your bank trading room'),
      '#required' => TRUE,
    );

    $form['phongdep_bank_global']['phongdep_bank_branch'] = array(
      '#type' => 'textfield',
      '#title' => t('Branch'),
      '#default_value' => variable_get('phongdep_bank_branch', NULL),
      '#description' => t('Your account branch'),
      '#required' => TRUE,
    );

    $form['phongdep_bank_global']['phongdep_bank_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank ID'),
      '#default_value' => variable_get('phongdep_bank_id', NULL),
      '#description' => t('Your bank account ID'),
      '#required' => TRUE,
    );

    $form['phongdep_bank_global']['phongdep_bank_account'] = array(
      '#type' => 'textfield',
      '#title' => t('Bank account'),
      '#default_value' => variable_get('phongdep_bank_account', NULL),
      '#description' => t('Your bank account name'),
      '#required' => TRUE,
    );

    return system_settings_form($form);
  }