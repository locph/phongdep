<?php

function cmp_remaining_day ($i, $j) {
  $a = $i['status'];
  $b = $j['status'];

  return $a == $b ? 0 : $a > $b ? 1 : -1;
}

function phongdep_ws_free_rooms () {
  global $base_url;

  // validate secret key.
  phongdep_ws_authenticate();

  // get free rooms
  $results = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'room', '=')
    ->execute()
    ->fetchCol();

  $rooms = node_load_multiple($results);

  $free_rooms = array();
  foreach ($rooms as $key => $value) {
    // rental duration
    $from = isset($value->field_rental_duration['und'][0]) ? $value->field_rental_duration['und'][0]['value'] : null;
    $to = isset($value->field_rental_duration['und'][0]['value2']) ? $value->field_rental_duration['und'][0]['value2'] : null;
    $remaining_day = $to ? phongdep_remain_date($to) : 0;

    if ($remaining_day <= 90) {
      $house = node_load($value->field_house['und'][0]['target_id']);
      if ($house) {
        $district = node_load($house->field_district['und'][0]['target_id']);
        $city = taxonomy_term_load($district->field_city['und'][0]['tid']);
        $free_rooms[$value->title] = array(
          "url"     => url("node/" . $value->nid),
          "image"   => file_create_url($house->field_images['und'][0]['uri']), // default image
          "area"    => $value->field_area['und'][0]['value'],
          "price"   => $value->field_price['und'][0]['value'],
          "status"  => $remaining_day, // status = 0 -> free, status = 5 -> leased and expired in 5 days
          "address" => $house->field_address['und'][0]['value'],
          "district"=> $district->title,
          "city"    => $city->name,
          "location" => array (
            "longitude" => $house->field_longitude['und'][0]['value'],
            "latitude"  => $house->field_latitude['und'][0]['value'],
            "altitude"  => $house->field_altitude['und'][0]['value']
          )
        );
      }
    }
  }

  uasort($free_rooms, 'cmp_remaining_day');

  $offset = isset($_GET['offset']) ? $_GET['offset'] : 0;
  $limit = isset($_GET['limit']) ? $_GET['limit'] : null;

  if ($limit) {
    $free_rooms = $offset ? array_slice($free_rooms, $offset, $limit) : array_slice($free_rooms, 0, $limit);
  }
  else {
    $free_rooms = $offset ? array_slice($free_rooms, $offset) : $free_rooms;
  }

  phongdep_return_json_data(API_STATUS_CODE_SUCCESS, 'Success', $free_rooms);
}