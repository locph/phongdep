<?php
/**
 * @file
 * my_feature.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function my_feature_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:<front>
  $menu_links['main-menu:<front>'] = array(
    'menu_name' => 'main-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Home',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
  );
  // Exported menu link: main-menu:http://caphemanhchophaimanh.vn/ranking
  $menu_links['main-menu:http://caphemanhchophaimanh.vn/ranking'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'http://caphemanhchophaimanh.vn/ranking',
    'router_path' => '',
    'link_title' => 'CFV',
    'options' => array(
      'attributes' => array(
        'title' => 'nescafe cafeviet',
        'id' => 'menu-cfv',
        'name' => 'i-dont-know',
        'rel' => 'nofollow',
        'class' => array(
          0 => 'cafeviet',
        ),
        'target' => '_blank',
        'accesskey' => 2,
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('CFV');
  t('Home');


  return $menu_links;
}
