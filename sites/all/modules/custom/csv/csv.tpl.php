<?php
/**
  * @file
  * Contact block template
  */

  /**
   * Convert CSV to array functions
   */
  function str_to_csv ($row) {
    if($row=='') {
      return array();
    }
    $a = array();
    $src = explode(';', $row );
    do {
      $p = array_shift($src);
      while (mb_substr_count($p,'"') % 2 != 0) {
        if (count($src)==0 ) {
          return false;
        }
        $p .= ',' . array_shift($src);
      }
      $match = null;
      if(preg_match('/^"(.+)"[\r\n]*$/', $p, $match)) {
        $p = $match[1];
      }
      $a[] = str_replace('""','"',$p);
    }
    while (count($src) > 0);

    return $a;
  }

  function file_getcsv ($f) {
    $line = fgets( $f );
    while( ($a = str_to_csv($line))===false ) {
      if( feof($f) ){  return false; }
      $line .= "\n". fgets( $f );
    }

    return $a;
  }

  function file_to_csv ($filename) {
    ini_set("auto_detect_line_endings", true);
    $a = array();
    $f = fopen($filename,'r');
    while (!feof($f)) {
      $rec = file_getcsv($f);
      if ($rec===false) {
        return false;
      }
      if (!empty($rec)) {
        $a[] = $rec;
      }
    }
    fclose($f);

    return $a;
  }

  function phongdep_csv_room () {
    $fileName = drupal_get_path('module', 'csv') . '/data/room_missing.csv';
    $datas = file_to_csv($fileName);

    // var_dump($datas);die;

    // $node = node_load(30);
    // var_dump($node);die;

    // add mail as array's key
    foreach ($datas as $key => $data) {
      // Create a node object, and add node properties.
      $node = (object) NULL;
      $node->title = $data[0];
      $node->type = 'room';
      $node->uid = 1;
      $node->created = strtotime("now");
      $node->changed = strtotime("now");
      $node->status = 1;
      $node->comment = 1;
      $node->promote = 0;
      $node->moderate = 0;
      $node->sticky = 0;
      $node->language = 'und';

      // info
      $node->field_price['und'][0]['value'] = $data[2];
      $node->field_area['und'][0]['value'] = $data[3];
      // search house
      $result = db_select('node', 'n')
        ->fields('n')
        ->condition('type', 'house', '=')
        ->condition('title', $data[5], '=')
        ->range(0,1)
        ->execute()
        ->fetch();
      $node->field_house['und'][0]['target_id'] = $result->nid;

      // contract
      $node->field_video['und'][0]['value'] = $data[1];
      $node->field_electricity_price['und'][0]['value'] = $data[6];
      $node->field_water_price['und'][0]['value'] = $data[7];
      $node->field_internet_price['und'][0]['value'] = $data[8];
      $node->field_washing_machine_price['und'][0]['value'] = $data[9];
      $node->field_cable_price['und'][0]['value'] = $data[10];
      $node->field_deposit['und'][0]['value'] = $data[11];
      $node->field_room_price['und'][0]['value'] = $data[12];
      $node->field_person['und'][0]['value'] = $data[13];
      $node->field_late_payment['und'][0]['value'] = $data[15];
      $node->field_rental_duration['und'][0]['value'] = $data[16];
      $node->field_rental_duration['und'][0]['value2'] = $data[17];
      $node->field_rental_duration['und'][0]['timezone'] = 'Asia/Bangkok';
      $node->field_rental_duration['und'][0]['timezone_db'] = 'Asia/Bangkok';
      $node->field_rental_duration['und'][0]['date_type'] = 'datetime';
      $node->field_elevator_price['und'][0]['value'] = $data[18];

      node_save($node);
      drupal_set_message('room ' . $node->title . ' created');
    }
  }

  function phongdep_csv_customer () {
    $fileName = drupal_get_path('module', 'csv') . '/data/customer.csv';
    $datas = file_to_csv($fileName);

    // var_dump($datas);die;

    // $node = node_load(87);
    // var_dump($node);die;

    // add mail as array's key
    foreach ($datas as $key => $data) {
      // Create a node object, and add node properties.
      $node = (object) NULL;
      $node->title = ucfirst($data[1]);
      $node->type = 'customer';
      $node->uid = 1;
      $node->created = strtotime("now");
      $node->changed = strtotime("now");
      $node->status = 1;
      $node->comment = 1;
      $node->promote = 0;
      $node->moderate = 0;
      $node->sticky = 0;
      $node->language = 'und';

      // find room
      $result = db_select('node', 'n')
        ->fields('n')
        ->condition('type', 'room', '=')
        ->condition('title', $data[2], '=')
        ->range(0,1)
        ->execute()
        ->fetch();
      if (isset($result->nid)) {
        $node->field_room['und'][0]['target_id'] = $result->nid;
      }

      $node->field_sexe['und'][0]['value'] = $data[11];
      $node->field_address['und'][0]['value'] = $data[7];
      $node->field_phone['und'][0]['value'] = $data[4];
      $node->field_email['und'][0]['value'] = $data[5];
      $node->field_identification['und'][0]['value'] = $data[6];
      // $node->field_city['und'][0]['tid'] = '';
      $node->field_birthday['und'][0]['value'] = $data[3];
      $node->field_birthday['und'][0]['timezone_db'] = 'Asia/Bangkok';
      $node->field_birthday['und'][0]['timezone_db'] = 'Asia/Bangkok';
      $node->field_birthday['und'][0]['date_type'] = 'datetime';

      node_save($node);
      drupal_set_message('customer ' . $node->title . ' created');
    }
  }

  // phongdep_csv_customer();

  // $room = node_load(89);
  // var_dump($room);die;

  $results = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->condition('type', 'customer', '=')
    ->execute()
    ->fetchCol();
  $results = node_load_multiple($results);

  // dsm($results);

  foreach ($results as $key => $value) {
    if (isset($value->field_room['und'])) {
      $room_id = $value->field_room['und'][0]['target_id'];
      $room = node_load($room_id);

      // update room info
      $room->field_customer_name['und'][0]['value'] = $value->title;
      $room->field_sexe['und'] = isset($value->field_sexe['und']) ? $value->field_sexe['und'] : null;
      $room->field_identification['und'] = isset($value->field_identification['und']) ? $value->field_identification['und'] : null;
      $room->field_birthday['und'] = isset($value->field_birthday['und']) ? $value->field_birthday['und'] : null;
      $room->field_phone['und'] = isset($value->field_phone['und']) ? $value->field_phone['und'] : null;
      $room->field_email['und'] = isset($value->field_email['und']) ? $value->field_email['und'] : null;
      $room->field_address['und'] = isset($value->field_address['und']) ? $value->field_address['und'] : null;
      $room->field_city['und'] = isset($value->field_city['und']) ? $value->field_city['und'] : null;

      node_save($room);
      // drupal_set_message('Customer ' . $value->title . ' has been imported to room ' $room->title);
      node_delete($value->nid);
      drupal_set_message('Customer ' . $value->title . ' has been removed.');
    }
  }
  // $room = node_load(37);
  // dsm($room);

  // dsm($rooms);

  // foreach ($rooms as $key => $room) {
  //   $experation_from = $room->field_rental_duration['und'][0]['value'];
  //   $experation_to = $room->field_rental_duration['und'][0]['value2'];
  //   echo $room->title . ': ' .$experation_from . ' vs ' . $experation_to . '<br>';
  //   echo strtotime($experation_from) . ' & ' . strtotime($experation_to) . '<br>';
  //   if (strtotime($experation_from) > strtotime($experation_to)) {
  //     // node_save($node);
  //     echo 'hell yeah';
  //     drupal_set_message('room ' . $node->title . ' has been updated. ');
  //   }
  // }